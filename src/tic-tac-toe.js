import React, { useState } from "react";

function TicTacToe() {
  const initialGrid = [
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ];
  const [grid, setGrid] = useState(initialGrid);
  const [currentPlayer, setCurrentPlayer] = useState("");
  const [winner, setWinner] = useState(null);
  const [startingPlayer, setStartingPlayer] = useState("");
  const [gameEnabled, setGameEnabled] = useState(false); // New state for game enabling

  const handleCellClick = (rowIndex, cellIndex) => {
    if (gameEnabled && grid[rowIndex][cellIndex] === null && !winner) {
      const newGrid = [...grid];
      newGrid[rowIndex][cellIndex] = currentPlayer;
      setGrid(newGrid);
      setCurrentPlayer(currentPlayer === "X" ? "O" : "X");
      checkWinner(newGrid);
    }
  };

  const checkWinner = (currentGrid) => {
    // Check rows
    for (let i = 0; i < 3; i++) {
      if (
        currentGrid[i][0] === currentGrid[i][1] &&
        currentGrid[i][1] === currentGrid[i][2] &&
        currentGrid[i][0] !== null
      ) {
        setWinner(currentGrid[i][0]);
        return;
      }
    }
    // Check columns
    for (let i = 0; i < 3; i++) {
      if (
        currentGrid[0][i] === currentGrid[1][i] &&
        currentGrid[1][i] === currentGrid[2][i] &&
        currentGrid[0][i] !== null
      ) {
        setWinner(currentGrid[0][i]);
        return;
      }
    }
    // Check diagonals
    if (
      currentGrid[0][0] === currentGrid[1][1] &&
      currentGrid[1][1] === currentGrid[2][2] &&
      currentGrid[0][0] !== null
    ) {
      setWinner(currentGrid[0][0]);
      return;
    }
    if (
      currentGrid[0][2] === currentGrid[1][1] &&
      currentGrid[1][1] === currentGrid[2][0] &&
      currentGrid[0][2] !== null
    ) {
      setWinner(currentGrid[0][2]);
      return;
    }

    // Check for a draw
    if (currentGrid.flat().every((cell) => cell !== null)) {
      setWinner("Draw");
    }
  };

  const handleRestartGame = () => {
    setGrid(initialGrid);
    setCurrentPlayer("");
    setWinner(null);
    setGameEnabled(false); // Enable game again on restart
  };

  const handleStartWith = (player) => {
    setStartingPlayer(player);
    setCurrentPlayer(player);
    setGameEnabled(true); // Disable game when a starting player is chosen
  };

  return (
    <div className="space-y-5">
      {winner ? (
        <p
          className={`text-center text-5xl font-extrabold ${
            winner === "X"
              ? "text-red-500"
              : winner === "O"
              ? "text-green-500"
              : "text-blue-500"
          }`}
        >
          {winner === "Draw" ? "DRAW!" : `${winner} WIN!`}
        </p>
      ) : (
        <p className="text-center text-5xl font-extrabold text-yellow-500">
          TIC-TAC-TOE GAME
        </p>
      )}

      <div className="flex justify-center">
        <button
          className={`bg-red-500 py-2 px-2 text-white font-extrabold text-3xl hover:bg-red-950 ${
            currentPlayer === "X" ? "bg-red-950" : "bg-red-500"
          }`}
          onClick={() => handleStartWith("X")}
          disabled={gameEnabled} // Disable button when the game is disabled
        >
          Start X
        </button>
        <button
          className={`bg-green-500 py-2 px-2 text-white font-extrabold text-3xl hover:bg-green-950 ${
            currentPlayer === "O" ? "bg-green-950" : "bg-green-500"
          }`}
          onClick={() => handleStartWith("O")}
          disabled={gameEnabled} // Disable button when the game is disabled
        >
          Start O
        </button>
      </div>
      <div className="flex flex-col items-center space-y-3 justify-center">
        <div>
          {grid.map((row, rowIndex) => (
            <div key={rowIndex} className="grid grid-cols-3">
              {row.map((cell, cellIndex) => (
                <span
                  key={cellIndex}
                  className={`w-20 h-20 flex items-center justify-center text-7xl font-extrabold bg-blue-500 border text-slate-950 border-blue-950 cursor-pointer`}
                  onClick={() => handleCellClick(rowIndex, cellIndex)}
                >
                  {cell === "X" && (
                    <p className="bg-red-500 font-extrabold w-20 h-20 text-center">
                      X
                    </p>
                  )}
                  {cell === "O" && (
                    <p className="bg-green-500 font-extrabold w-20 h-20 text-center">
                      O
                    </p>
                  )}
                </span>
              ))}
            </div>
          ))}
        </div>
        <button
          className="bg-red-950 py-3 px-3 text-white rounded-full font-extrabold text-3xl hover:bg-red-500"
          onClick={handleRestartGame}
        >
          RESTART
        </button>
      </div>
    </div>
  );
}

export default TicTacToe;
