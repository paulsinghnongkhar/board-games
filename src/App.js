import React, { useState } from "react";
import TicTacToe from "./tic-tac-toe";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import Bingo from "./bingo";
import "./index.css";

function App() {
  const [activeTab, setActiveTab] = useState(0);

  return (
    <div style={{ backgroundColor: "#FEE2E2" }} className="flex justify-center h-screen">
      <Tabs className="space-y-10 lg:space-y-5 lg:py-3 py-10">
        <TabList
          selectedIndex={activeTab}
          onSelect={setActiveTab}
          className="flex justify-center text-xl text-white font-extrabold text-center"
        >
          <Tab
            className={`py-2 px-10 bg-blue-950 border-r cursor-pointer hover:bg-blue-500 ${
              activeTab === 0 ? "activetab" : ""
            }`}
            onClick={() => setActiveTab(0)}
          >
            BINGO GAME
          </Tab>
          <Tab
            className={`p-2 bg-blue-950 border-l cursor-pointer hover:bg-blue-500 ${
              activeTab === 1 ? "activetab" : ""
            }`}
            onClick={() => setActiveTab(1)}
          >
            TIC-TAC-TOE GAME
          </Tab>
        </TabList>
        <TabPanel>
          <Bingo />
        </TabPanel>
        <TabPanel>
          <TicTacToe />
        </TabPanel>
      </Tabs>
    </div>
  );
}

export default App;
