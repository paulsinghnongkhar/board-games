import {
  faClock,
  faRefresh,
  faSadTear,
  faSmile,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState, useEffect } from "react";

function Bingo() {
  const initialGrid = [
    [null, null, null, null, null],
    [null, null, null, null, null],
    [null, null, null, null, null],
    [null, null, null, null, null],
    [null, null, null, null, null],
  ];

  const [grid, setGrid] = useState(initialGrid);
  const [randomNumber, setRandomNumber] = useState(0);
  const [bingo, setBingo] = useState("");
  const [gameover, setGameOver] = useState("");
  const [timer, setTimer] = useState({ minutes: "", seconds: "" }); // Modified
  const [score, setScore] = useState(0);
  const [startClicked, setStartClicked] = useState(false); // New state variable to track if start button is clicked

  useEffect(() => {
    if (startClicked && !gameover && !bingo) {
      const totalSeconds = timer.minutes * 60 + timer.seconds;
      if (totalSeconds > 0) {
        const intervalId = setInterval(() => {
          setTimer((prevTimer) => {
            if (prevTimer.seconds === 0 && prevTimer.minutes === 0) {
              clearInterval(intervalId);
              return prevTimer;
            } else if (prevTimer.seconds === 0) {
              return { minutes: prevTimer.minutes - 1, seconds: 59 };
            } else {
              return {
                minutes: prevTimer.minutes,
                seconds: prevTimer.seconds - 1,
              };
            }
          });
        }, 1000);

        return () => clearInterval(intervalId);
      } else {
        setGameOver("GAME OVER!");
        setRandomNumber(0);
      }
    }
  }, [timer, bingo, gameover, startClicked]);

  useEffect(() => {
    const intervalId = setInterval(() => {
      if (startClicked && !gameover && !bingo) {
        const newRandomNumber = Math.floor(Math.random() * 25) + 1;
        setRandomNumber(newRandomNumber);
        // Start the timer only when start button is clicked and a random number is generated
        if (timer.minutes === 0 && timer.seconds === 0) {
          setTimer({ minutes: 3, seconds: 0 });
        }
      }
    }, 3000);

    return () => clearInterval(intervalId);
  }, [gameover, bingo, startClicked]);

  const generateRandomNumber = () => {
    if (!startClicked) {
      setStartClicked(true);
      setTimer({ minutes: 3, seconds: 0 }); // Start the timer
    }

    let generatedNumbers = new Set();
    const newGrid = grid.map((row, i) =>
      row.map((cell, j) => {
        if (cell === randomNumber) {
          return "X";
        } else if (cell === null) {
          let newNumber;

          do {
            newNumber = Math.floor(Math.random() * 25) + 1;
          } while (generatedNumbers.has(newNumber));

          generatedNumbers.add(newNumber);
          return newNumber;
        } else {
          return cell;
        }
      })
    );

    let isBingo = checkForBingo(newGrid);
    setGrid(newGrid);

    if (isBingo) {
      if (bingo !== "BINGO!") {
        setBingo("BINGO!");
        setTimer(timer); // Stop the timer
        setRandomNumber(0);
      }
      setScore(9);
    } else {
      let score = 0;
      for (let i = 0; i < 5; i++) {
        if (newGrid[i].every((cell) => cell === "X")) {
          score++;
        }
        if (newGrid.every((row) => row[i] === "X")) {
          score++;
        }
      }
      setScore(score);
    }

    if (!isBingo) {
      const oneRandomNumber = Math.floor(Math.random() * 25) + 1;
      setRandomNumber(oneRandomNumber);
    }
  };

  const generateRandomNumberAtCell = (rowIndex, cellIndex) => {
    const selectedNumber = grid[rowIndex][cellIndex];
    if (selectedNumber === randomNumber) {
      generateRandomNumber();
    }
  };

  const checkForBingo = (grid) => {
    let rowCount = 0;
    let colCount = 0;

    for (let i = 0; i < 5; i++) {
      if (grid[i].every((cell) => cell === "X")) {
        rowCount++;
      }
      if (grid.every((row) => row[i] === "X")) {
        colCount++;
      }
    }

    if (rowCount === 5 || colCount === 5) {
      return true;
    }

    return false;
  };

  const handleRestartGame = () => {
    setGameOver("");
    setBingo("");
    setGrid(initialGrid);
    setRandomNumber(0);
    setScore(0);
    setTimer({ minutes: "", seconds: "" }); // Modified
    setStartClicked(false);
  };

  return (
    <div className="space-y-5">
      {bingo === "BINGO!" ? (
        <p className="text-center text-5xl font-extrabold text-green-500">
          BINGO!
        </p>
      ) : gameover === "GAME OVER!" ? (
        <p className="text-center text-5xl font-extrabold text-red-500">
          GAME OVER!
        </p>
      ) : (
        <p className="text-center text-5xl font-extrabold text-yellow-500">
          BINGO GAME
        </p>
      )}
      <div className="space-y-5">
        <div className="space-y-5">
          <div className="flex justify-center gap-44">
            <p
              className={`lg:text-5xl text-3xl font-extrabold ${
                score >= 1 && score <= 3
                  ? "text-red-500"
                  : score >= 4 && score <= 6
                  ? "text-blue-500"
                  : score >= 7 && score <= 9
                  ? "text-green-500"
                  : ""
              }`}
            >
              <FontAwesomeIcon icon={faStar} className="pr-1" />
              {score}
            </p>
            <p
              className={`lg:text-5xl text-3xl font-extrabold ${
                timer.minutes === 0 && timer.seconds <= 30 ? "text-red-700" : ""
              }`}
            >
              {`${timer.minutes.toString().padStart(2, "0")}:${timer.seconds
                .toString()
                .padStart(2, "0")}`}
              <FontAwesomeIcon icon={faClock} className="pl-1" />
            </p>
          </div>
          <div>
            {bingo ? (
              <center>
                <FontAwesomeIcon
                  icon={faSmile}
                  className="text-5xl text-green-500"
                />
              </center>
            ) : gameover ? (
              <center>
                <FontAwesomeIcon
                  icon={faSadTear}
                  className="text-5xl text-red-500"
                />
              </center>
            ) : (
              <center>
                <div className="text-center text-5xl font-extrabold text-white bg-blue-950 w-fit rounded px-5 lg:pb-2">
                  {randomNumber}
                </div>
              </center>
            )}
          </div>
        </div>
        <div>
          {grid.map((row, rowIndex) => (
            <div key={rowIndex} className="grid grid-cols-5 lg:mx-[10%] mx-3">
              {row.map((cell, cellIndex) => (
                <span
                  key={cellIndex}
                  className={`py-3 text-wh text-5xl font-extrabold bg-blue-500 border text-white border-blue-950 cursor-pointer text-center ${
                    cell === null
                      ? "py-9"
                      : bingo
                      ? "bg-green-500"
                      : gameover
                      ? "bg-red-500"
                      : ""
                  }`}
                  onClick={
                    cell === null
                      ? null
                      : () => generateRandomNumberAtCell(rowIndex, cellIndex)
                  }
                >
                  {cell === "X" && (
                    <p className="text-red-500 font-extrabold">X</p>
                  )}
                  {cell !== "X" && cell}
                </span>
              ))}
            </div>
          ))}
        </div>
        <center>
          <div className="flex flex-col gap-y-2 w-40">
            {!startClicked && !bingo && !gameover && (
              <button
                className="bg-blue-950 py-3 px-3 text-white rounded-full font-extrabold text-3xl hover:bg-blue-500"
                onClick={generateRandomNumber}
              >
                START
              </button>
            )}
            {grid.every((row) => row.every((cell) => cell !== null)) && (
              <button
                className="bg-red-950 py-3 px-3 text-white rounded-full font-extrabold text-3xl hover:bg-red-500"
                onClick={handleRestartGame}
              >
                RESTART
              </button>
            )}
          </div>
        </center>
      </div>
    </div>
  );
}

export default Bingo;
